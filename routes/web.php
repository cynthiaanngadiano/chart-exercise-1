<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/highcharts/demo/1/', ['as' => 'highcharts_demo_1', 'uses' => 'chartsController@demo1']);
Route::get('/highcharts/demo/2/', ['as' => 'highcharts_demo_2', 'uses' => 'chartsController@demo2']);
