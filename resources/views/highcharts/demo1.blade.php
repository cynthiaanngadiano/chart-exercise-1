<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>HighChart Activities</title>
    <script src="https://code.highcharts.com/highcharts.js"></script>
</head>
    <body>
      <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto;"></div>
      <script type="text/javascript">
        Highcharts.chart('container', {
              chart: {
                  type: 'column'
              },
              title: {
                  text: 'Economic Profit %',
                  align: 'left',
                  style: { fontWeight: 'bold',
                  }
              },
              xAxis: {
                  categories: ['2011', '2012', '2013', '2014', '2015']
              },
              yAxis: {
                  min: 0,
                  title: {
                      text: ''
                  },
                  stackLabels: {
                      enabled: false
                  }
              },
              legend: {
                  reversed: true
              },
              tooltip: {
                  pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b><br/>',
                  shared: true
              },
              plotOptions: {
                  column: {
                      stacking: 'normal'
                  }
              },
              series: [{
                  name: 'Kenneth',
                  data: [12, 15, 4, 16, 9],
                  color: '#D8692F'
              }, {
                  name: 'Ed',
                  data: [8, 22, 9, 12, 7],
                  color: '#e0885a'
              }, {
                  name: 'Gillian',
                  data: [19, 5, 1, 21, 18],
                  color: '#DA524C'
              }]
            });
      </script>
    </body>
</html>
