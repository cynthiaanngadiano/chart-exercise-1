<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <title>HighChart Activities</title>
    <script src="https://code.highcharts.com/highcharts.js"></script>
</head>
    <body>
      <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto;"></div>
      <script type="text/javascript">
        Highcharts.chart('container', {
          chart: {
            type: 'column'
          },
          title: {
            text: 'Weight Gained %',
            align: 'left',
            style: {
              fontWeight: 'bold',
            }
          },
          xAxis: {
            categories: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']
          },
          yAxis: {
            min: 0,
            title: {
              text: ''
            },
            labels: {
              formatter: function() {
                return '<b>' + Highcharts.numberFormat((this.value * 100), 2, '.') + '% </b>';
              }
            },
            stackLabels: {
              enabled: false
            }
          },
          legend: {
            reversed: true
          },
          tooltip: {
            formatter: function() {
              return '<span style="color: ' + this.series.color + '">' + this.series.name +
                '</span>: <b>' + Highcharts.numberFormat((this.y * 100), 2, '.') + '%</b>';
            }
          },
          plotOptions: {
            column: {
              stacking: 'normal'
            }
          },
          series: [{
            name: 'Papa Bear',
            data: [0.124, 0.6693, 1.252, 0.3463, 0.183],
            color: '#365f8c'
          }, {
            name: 'Mama Bear',
            data: [0.997, 0.111, 0.637, 0.551, 1.116],
            color: '#da577c'
          }]
        });
      </script>
    </body>
</html>
